const express = require('express')
const router = express.Router()
const db = require('../config/db')
const Gig = require('../models/Gig')
const Sequelize = require('sequelize')
const Op = Sequelize.Op


// Get Gig list
/**
 * return all the gigs
 */
router.get('/', async (req, res) => {
    try {
        const gigs = await Gig.findAll()
        return res.render('gigs', { gigs })
    } catch (err) {
        console.log(err)
        return res.status(500).message({ code: 'Error while getting all the data' })
    }


    //WITHOUT USING THE ASYNC AWAIT FUNCTION
    //Gig.findAll()
        //.then(gigs => {
            //res.render('gigs', {
                //gigs
            //})
        //})
        //.catch(err => console.log('Err: ' + err))


});


// Display add gig form
router.get('/add', (req, res) => {
    res.render('add')
})


/**
 * When we add a new gig, we first want to be sure that the users enter all the input, if not we give him an error msg and send back all the data that he already put
 */
// Add a gig
router.post('/add', async (req, res) => {

    try{

        let { title, technologies, budget, description, contact_email} = req.body
        let errors = [];

        if(!title) {
            errors.push({ text: 'Please add a title '})
        }
        if(!technologies) {
            errors.push({ text: 'Please add a technology'})
        }
        if(!description) {
            errors.push({ text: 'Please add a description '})
        }
        if(!contact_email) {
            errors.push({ text: 'Please add a contact email  '})
        }


        // Check for errors
        if(errors.length > 0){
            res.render('add', {
                errors,
                title,
                technologies,
                description,
                budget,
                contact_email

            })
        }
        else {
            if (!budget) {
                budget = 'Unknown'
            } else {
                budget = `${budget} €`
            }

            //Make lowercase and remove space after comma
            technologies = technologies.toLowerCase().replace(/, /g, ',')


            // Insert into table
            await Gig.create({
                title,
                technologies,
                description,
                budget,
                contact_email
            })

            return res.redirect('/gigs')
        }
    }catch(err){
        console.log(err)
        return res.status(500).message({ code : 'Problem when adding a new client ' })
    }

        //WITHOUT USING THE ASYNC AWAIT FUNCTION
        //Gig.create({
        //             title,
        //             technologies,
        //             description,
        //             budget,
        //             contact_email
        //         })
        //.then(gig => res.redirect('/gigs'))
        //.catch(err => console.log('Err: ' + err))
})

/**
 *
 */
router.get('/search', async (req, res) => {
    try {
        let { term } = req.query

        // Make lowercase
        term = term.toLowerCase()
        const gigs = await Gig.findAll({ where: { technologies: { [Op.like]: '%' + term + '%'}}})

        return res.render('gigs', { gigs })
    } catch (err) {
        console.log(err)
        return res.status(500).message({ code: 'problem' })
    }



})

module.exports = router;

