const express = require('express')
const exphbs = require('express-handlebars')
const body = require('body-parser')
const path = require('path')


//DB
const db = require('./config/db')


//Test db
db.authenticate()
    .then(() => console.log('Database connected'))
    .catch(err => console.log('Error: ' + err))

const app =  express();

// Handlebars
app.engine('handlebars', exphbs({ defaultLayout: 'main'}))
app.set('view engine', 'handlebars')

// Body Parser
app.use(body.urlencoded({ extended: false}))

// Set Static folder
app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) =>{
    res.render('index', { layout: 'landing'})
})


// Gig routes
app.use('/gigs', require('./routes/gigs'))
const PORT = process.env.PORT  || 3000


app.listen(PORT, console.log(`Server strating on port: ${PORT}`))